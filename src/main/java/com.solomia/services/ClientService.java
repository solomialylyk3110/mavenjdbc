package com.solomia.services;

import com.solomia.connector.DBManager;
import com.solomia.model.Client;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientService implements Service<Client> {
    private static Logger logger = LogManager.getLogger(ClientService.class);
    private static final String CREATE = "INSERT INTO client VALUES (?, ?, ?)";
    private static final String UPDATE = "UPDATE client SET client_name=?, client_surname=? WHERE client_id=?";
    private static final String DELETE = "DELETE FROM client WHERE client_id=?";
    private static final String GET_ALL = "SELECT * FROM client";
    private static final String SELECT = "SELECT * FROM client where client_id=?";

    @Override
    public void create(Client entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1,entity.getClientId());
            ps.setString(2,entity.getClientName());
            ps.setString(3,entity.getClientSurname());
            ps.executeUpdate();
        }
    }

    public void update(Client entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setInt(1,entity.getClientId());
            ps.setString(2,entity.getClientName());
            ps.setString(3,entity.getClientSurname());
            ps.executeUpdate();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1,id);
            ps.executeUpdate();
        }
    }

    public void deleteInAll(int id) throws SQLException {
        new ClientContractService().deleteByClient(id);
        delete(id);
    }


    @Override
    public void select() throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(GET_ALL);
        ResultSet resultSet = ps.executeQuery();
        List<Client> clientList = new ArrayList<>();
        while (resultSet.next()) {
            clientList.add(new Client(resultSet.getInt("client_id"),
                    resultSet.getString("client_name"), resultSet.getString("client_surname")));
        }
        logger.info(clientList);
    }

    public boolean selectById(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(SELECT);
        ps.setInt(1,id);
        ResultSet resultSet = ps.executeQuery();
        List<Client> clientList = new ArrayList<>();
        while (resultSet.next()) {
            clientList.add(new Client(resultSet.getInt("client_id"),
                    resultSet.getString("client_name"), resultSet.getString("client_surname")));
        }
        logger.info(clientList);
        return false;
    }
}
