package com.solomia.services;

import com.solomia.connector.DBManager;
import com.solomia.model.ClientContract;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ClientContractService  {
    private static Logger logger = LogManager.getLogger(ClientContractService.class);
    private static final String CREATE = "INSERT INTO client_contract VALUES (?, ?)";
    private static final String UPDATE = "UPDATE client_contract SET client_id=?, contract_id=? WHERE contract_id=? and client_id=?";
    private static final String DELETE = "DELETE FROM client_contract WHERE client_id=? and WHERE contract_id=?";
    private static final String DELETEBYCLIENT = "DELETE FROM client_contract WHERE client_id=?";
    private static final String DELETEBYCONTRACT = "DELETE FROM client_contract WHERE contract_id=?";
    private static final String GET_ALL = "SELECT * FROM client_contract";

    public void create(ClientContract entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1,entity.getClientId());
            ps.setInt(2,entity.getContractId());
            ps.executeUpdate();
        }
    }

    public void update(ClientContract entity, int client_id, int contract_id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setInt(1,entity.getClientId());
            ps.setInt(2,entity.getContractId());
            ps.setInt(3,client_id);
            ps.setInt(4, contract_id);
            ps.executeUpdate();
        }
    }

    public void delete(int client_id, int contract_id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1, client_id);
            ps.setInt(2, contract_id);
            ps.executeUpdate();
        }
    }

    public void deleteByClient(int client_id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETEBYCLIENT)) {
            ps.setInt(1, client_id);
            ps.executeUpdate();
        }
    }

    public void deleteByContract(int client_id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETEBYCONTRACT)) {
            ps.setInt(1, client_id);
            ps.executeUpdate();
        }
    }

    public void select() throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(GET_ALL);
        ResultSet resultSet = ps.executeQuery();
        List<ClientContract> list = new ArrayList<>();
        while (resultSet.next()) {
            list.add(new ClientContract(resultSet.getInt("client_id"),
                    resultSet.getInt("contract_id")));
        }
        logger.info(list);
    }

}
