package com.solomia.services;

import com.solomia.connector.DBManager;
import com.solomia.model.Realtor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RealtorService implements Service<Realtor>{
    private static Logger logger = LogManager.getLogger(RealtorService.class);
    private static final String CREATE = "INSERT INTO realtor VALUES (?, ?, ?)";
    private static final String UPDATE = "UPDATE realtor SET realtor_name=?, realtor_surname=? WHERE realtor_id=?";
    private static final String DELETE = "DELETE FROM realtor WHERE realtor_id=?";
    private static final String DELETEBYCONTRACT = "DELETE FROM realtor WHERE contract_id=?";
    private static final String GET_ALL = "SELECT * FROM realtor";
    private static final String SELECT = "SELECT * FROM realtor where realtor_id=?";

    @Override
    public void create(Realtor entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1,entity.getRealtorId());
            ps.setString(2,entity.getRealtorName());
            ps.setString(3,entity.getRealtorSurname());
            ps.executeUpdate();
        }
    }

    public void update(Realtor entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setInt(1,entity.getRealtorId());
            ps.setString(2,entity.getRealtorName());
            ps.setString(3,entity.getRealtorSurname());
            ps.executeUpdate();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1,id);
            ps.executeUpdate();
        }
    }

    public void deleteByContract(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETEBYCONTRACT)) {
            ps.setInt(1,id);
            ps.executeUpdate();
        }
    }

    public boolean selectById(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(SELECT);
        ps.setInt(1,id);
        ResultSet resultSet = ps.executeQuery();
        List<Realtor> realtorList = new ArrayList<>();
        while (resultSet.next()) {
            realtorList.add(new Realtor(resultSet.getInt("realtor_id"),
                    resultSet.getString("realtor_name"), resultSet.getString("realtor_surname")));
        }
        logger.info(realtorList);
        return false;
    }

    @Override
    public void select() throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(GET_ALL);
        ResultSet resultSet = ps.executeQuery();
        List<Realtor> realtorList = new ArrayList<>();
        while (resultSet.next()) {
            realtorList.add(new Realtor(resultSet.getInt("realtor_id"),
                    resultSet.getString("realtor_name"), resultSet.getString("realtor_surname")));
        }
        logger.info(realtorList);
    }
}
