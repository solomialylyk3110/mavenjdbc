package com.solomia.services;

import com.solomia.connector.DBManager;
import com.solomia.model.Payment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PaymentService implements Service<Payment>{
    private static Logger logger = LogManager.getLogger(PaymentService.class);
    private static final String CREATE = "INSERT INTO payment VALUES (?, ?, ?)";
    private static final String UPDATE = "UPDATE payment SET payment_month=?, payment_year=? WHERE payment_id=?";
    private static final String DELETE = "DELETE FROM payment WHERE payment_id=?";
    private static final String DELETEBYCONTRACT = "DELETE FROM payment WHERE contract_id=?";
    private static final String GET_ALL = "SELECT * FROM payment";
    private static final String SELECT = "SELECT * FROM payment where payment_id=?";

    @Override
    public void create(Payment entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
            ps.setInt(1,entity.getPaymentId());
            ps.setInt(2,entity.getPaymentMonth());
            ps.setInt(3,entity.getPaymentYear());
            ps.executeUpdate();
        }
    }

    public void update(Payment entity) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
            ps.setInt(1,entity.getPaymentId());
            ps.setInt(2,entity.getPaymentMonth());
            ps.setInt(3,entity.getPaymentYear());
            ps.executeUpdate();
        }
    }

    @Override
    public void delete(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
            ps.setInt(1,id);
            ps.executeUpdate();
        }
    }

    public void deleteByContract(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(DELETEBYCONTRACT)) {
            ps.setInt(1,id);
            ps.executeUpdate();
        }
    }

    @Override
    public void select() throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(GET_ALL);
        ResultSet resultSet = ps.executeQuery();
        List<Payment> paymentList = new ArrayList<>();
        while (resultSet.next()) {
            paymentList.add(new Payment(resultSet.getInt("payment_id"),
                    resultSet.getInt("payment_month"), resultSet.getInt("payment_year")));
        }
        logger.info(paymentList);
    }

    public boolean selectById(int id) throws SQLException {
        Connection connection = DBManager.getConnection();
        PreparedStatement ps = connection.prepareStatement(SELECT);
        try {
            ps.setInt(1,id);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        ResultSet resultSet = ps.executeQuery();
        List<Payment> paymentList = new ArrayList<>();
        while (resultSet.next()) {
            paymentList.add(new Payment(resultSet.getInt("payment_id"),
                    resultSet.getInt("payment_month"), resultSet.getInt("payment_year")));
        }
        logger.info(paymentList);
        return true;
    }
}
