package com.solomia.factory;

import com.solomia.TypeEntity;
import com.solomia.services.*;

public class MyFactory extends ServiceFactory {

    @Override
    protected Service createFactory(TypeEntity type) {
        Service service = null;
        if (type == TypeEntity.CLIENT) {
            service = new PaymentService();
        } else if (type == TypeEntity.CONTRACT) {
            service = new ContractService();
        } else if (type == TypeEntity.OPERATION) {
            service = new OperationService();
        } else if (type == TypeEntity.PAYMENT) {
            service = new PaymentService();
        } else if (type == TypeEntity.REALTOR) {
            service = new RealtorService();
        }
        return service;
    }
}
