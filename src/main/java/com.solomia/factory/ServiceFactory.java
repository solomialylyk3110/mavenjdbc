package com.solomia.factory;

import com.solomia.TypeEntity;
import com.solomia.services.Service;

public abstract class ServiceFactory {
    protected abstract Service createFactory(TypeEntity type);

    public Service createService(TypeEntity type) {
        Service service = createFactory(type);
        return service;
    }
}
