package com.solomia.model;

public class Realtor {
    private Integer realtorId;
    private String realtorName;
    private String realtorSurname;

    public Realtor(Integer realtorId, String realtorName, String realtorSurname) {
        this.realtorId = realtorId;
        this.realtorName = realtorName;
        this.realtorSurname = realtorSurname;
    }

    public Integer getRealtorId() {
        return realtorId;
    }

    public void setRealtorId(Integer realtorId) {
        this.realtorId = realtorId;
    }

    public String getRealtorName() {
        return realtorName;
    }

    public void setRealtorName(String realtorName) {
        this.realtorName = realtorName;
    }

    public String getRealtorSurname() {
        return realtorSurname;
    }

    public void setRealtorSurname(String realtorSurname) {
        this.realtorSurname = realtorSurname;
    }

    @Override
    public String toString() {
        return "Realtor{" +
                "realtorId=" + realtorId +
                ", realtorName='" + realtorName + '\'' +
                ", realtorSurname='" + realtorSurname + '\'' +
                '}';
    }
}
