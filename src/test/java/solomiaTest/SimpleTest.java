package solomiaTest;

import com.solomia.TypeEntity;
import com.solomia.factory.MyFactory;
import com.solomia.factory.ServiceFactory;
import com.solomia.services.Service;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;

public class SimpleTest {
    @BeforeEach
    void before() {
        System.out.println("Before each test");
    }

    @Test
    void checkSelectById() throws SQLException {
        ServiceFactory serviceFactory = new MyFactory();
        Service servicePayment = serviceFactory.createService(TypeEntity.PAYMENT);
        if(servicePayment.selectById(1)){
            System.out.println("Test is successful");
        }
    }

    @AfterEach
    void after() {
        System.out.println('\n'+"After each test");
    }
}
