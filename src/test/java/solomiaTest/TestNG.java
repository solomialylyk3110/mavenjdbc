package solomiaTest;

import com.solomia.constant.Constants;
import com.solomia.model.Operation;
import com.solomia.services.OperationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TestNG {
    private static Logger logger = LogManager.getLogger(TestNG.class);
    private static Connection connection = null;

    @Test(priority = 1)
    public void test1() {
        logger.info("First test");
    }

//    @Test(priority = 2)
//    public void checkIfEmptyOperation() throws SQLException {
//        logger.info("Check first id");
//        OperationService operationService = new OperationService();
//        operationService.selectById(10);

//        if (operationService.selectById(1)!=null) {
//            logger.info("Test is successful");
//        } else {
//            logger.error("Do not exist by id");
//        }
//    }

    @Test(dependsOnMethods = "checkConnection", alwaysRun = true)
    void checkIfCanAddSameId() {
        logger.info("Test2. Trying to add with the same id");
        Boolean available =true;
        OperationService operationService = new OperationService();
        try {
            logger.info("Add operation with id=1");
            operationService.create(new Operation(1, "Operation"));
        } catch (SQLException e) {
            logger.info("available = false");
            available = false;
        }
        Assert.assertNotEquals(available, true);
    }

    @Test(priority = 2)
    public void checkConnection() {
        logger.info("Test checkConnection");
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(Constants.url, Constants.user, Constants.password);
            } catch (SQLException e) {
                logger.error("SQLException: " + e.getMessage());
                logger.error("SQLState: " + e.getSQLState());
                logger.error("VendorError: " + e.getErrorCode());
            }
        }
        try {
            if (!connection.isClosed()) {
                logger.info("Success");
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        Assert.assertNotEquals(connection, null);
    }

}
